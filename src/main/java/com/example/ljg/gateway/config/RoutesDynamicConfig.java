package com.example.ljg.gateway.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.cloud.gateway")
public class RoutesDynamicConfig {

    private Routes routes;

    @Data
    public class Routes{
        private List<String> id;

        private String uri;

        private List<String> predicates;

        private List<String> filters;
    }

}
